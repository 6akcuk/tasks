#!/usr/bin/env bash

cp -rf ./.env.example ./.env

docker-compose build
docker-compose up -d mysql redis
docker-compose run php bash -c "cd /app && rm -rf vendor && composer install";
docker-compose run php bash -c "cd /app && php artisan key:generate && php artisan queue:failed-table && php artisan migrate && php artisan db:seed"