@extends('layouts.app')

@section('content')
    @if (session()->has('message'))
        <div style="padding-bottom: 20px;">
            {{ session()->get('message') }}
        </div>
    @endif

    <form action="{{ route('tasks.store') }}" method="post">
        {{ csrf_field() }}

        @if ($errors->count())
            {{ dump($errors->all()) }}
        @endif

        <input type="text" name="url" placeholder="Resource URL" required>
        <input type="submit" value="Download">
    </form>

    <table border="1">
        <thead>
        <tr>
            <th>#</th>
            <th>Date/Time</th>
            <th>Resource</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($tasks as $task)
            <tr>
                <td>{{ $task->id }}</td>
                <td>{{ $task->created_at->format('Y-m-d H:i:s') }}</td>
                <td>{{ $task->url }}</td>
                <td>{{ $task->status }}</td>
                <td>
                    @if ($task->isPending())
                    <form id="delete-{{ $task->id }}" action="{{ route('tasks.destroy', $task) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>

                    <a href="#" onclick="document.getElementById('delete-{{ $task->id }}').submit()" style="color: red">
                        Remove
                    </a>
                    @endif

                    @if ($task->isComplete())
                        <a href="{{ $task->download_url }}">Download</a>
                    @endif

                    @if ($task->isError())
                        {{ $task->error_message }}
                    @endif
                </td>
            </tr>
        @empty
            <tr>
                <td align="center" colspan="5">
                    <h2>Tasks not found.</h2>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection