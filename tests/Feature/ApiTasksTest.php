<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ApiTasksTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function can_see_all_tasks()
    {
        $task = factory('App\Task')->create();

        $response = $this->get('/api/tasks');
        $response->assertJsonFragment([
            'id' => $task->id,
            'url' => $task->url,
            'status' => $task->status
        ]);
    }

    /** @test */
    public function can_create_a_new_task()
    {
        $response = $this->json('POST', '/api/tasks', [
            'url' => 'https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'
        ]);

        $response->assertSuccessful()
            ->assertJson([
                'url' => 'https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'
            ]);

        $this->assertDatabaseHas('tasks', [
            'url' => 'https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'
        ]);
    }

    /** @test */
    public function can_delete_a_task()
    {
        $task = factory('App\Task')->create();

        $response = $this->delete('/api/tasks/'. $task->id);
        $response->assertSuccessful();

        $this->assertDatabaseMissing('tasks', [
            'id' => $task->id,
            'url' => $task->url,
            'status' => $task->status
        ]);
    }
}
