<?php

namespace Tests\Feature;

use App\Jobs\DownloadResource;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class TasksTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function can_see_all_tasks()
    {
        $task = factory('App\Task')->create(['status' => 'pending']);

        $response = $this->get('/tasks');
        $response->assertSee($task->url);
    }

    /** @test */
    public function can_create_a_new_task()
    {
        Queue::fake();

        $url = 'https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';

        $response = $this->post('/tasks', [
            'url' => $url
        ]);
        $response->assertRedirect('/');

        $this->assertDatabaseHas('tasks', [
            'url' => $url
        ]);

        Queue::assertPushed(DownloadResource::class);
    }

    /** @test */
    public function can_delete_a_task()
    {
        $task = factory('App\Task')->create();

        $this->delete('/tasks/'. $task->id);

        $this->assertDatabaseMissing('tasks', [
            'id' => $task->id,
            'url' => $task->url
        ]);
    }
}
