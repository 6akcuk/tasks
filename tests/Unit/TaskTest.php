<?php

namespace Tests\Unit;

use App\Task;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TaskTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Task
     */
    public $task;

    public function setUp()
    {
        parent::setUp();

        $this->task = factory('App\Task')->create();
    }

    /** @test */
    public function status_functions_is_correct()
    {
        $this->assertEquals($this->task->status == Task::STATUS_ERROR, $this->task->isError());

        $this->assertEquals($this->task->status == Task::STATUS_PENDING, $this->task->isPending());

        $this->assertEquals($this->task->status == Task::STATUS_DOWNLOADING, $this->task->isDownloading());

        $this->assertEquals($this->task->status == Task::STATUS_COMPLETE, $this->task->isComplete());
    }

    /** @test */
    public function generate_resource_url()
    {
        $extension = pathinfo($this->task->url, PATHINFO_EXTENSION);
        if ( ! $extension) {
            $extension = 'html';
        }

        $this->assertStringEndsWith($extension, $this->task->generateResourceName());

        $this->assertStringMatchesFormat('%x.'. $extension, $this->task->generateResourceName());
    }

    /** @test */
    public function download_url()
    {
        $this->assertStringEndsWith($this->task->resource, $this->task->download_url);
    }
}
