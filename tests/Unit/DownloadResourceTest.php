<?php

namespace Tests\Unit;

use App\Jobs\DownloadResource;
use App\Task;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DownloadResourceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Task
     */
    public $task;

    public function setUp()
    {
        parent::setUp();

        $this->task = factory('App\Task')->create(['status' => 'pending']);
    }

    /** @test */
    public function can_handle_downloading()
    {
        $this->task->url = 'https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';

        $job = new DownloadResource($this->task);
        $job->handle();

        $task = Task::find($this->task->id);

        $filepath = storage_path('app/public/'. $task->resource);
        $this->assertTrue($task->isComplete());
        $this->assertFileExists($filepath);

        Storage::delete($filepath);
    }

    /** @test */
    public function can_handle_error()
    {
        $job = new DownloadResource($this->task);
        $job->handle();

        $task = Task::find($this->task->id);

        $this->assertTrue($task->isError());
    }
}
