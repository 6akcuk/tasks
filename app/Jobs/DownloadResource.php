<?php

namespace App\Jobs;

use App\Task;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class DownloadResource implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $task;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->task->update([
            'status' => Task::STATUS_DOWNLOADING
        ]);

        $client = new Client();

        try {
            $response = $client->get($this->task->url);
            $filename = $this->task->generateResourceName();

            Storage::put($filename, $response->getBody()->getContents());

            $this->task->update([
                'status' => Task::STATUS_COMPLETE,
                'resource' => $filename
            ]);
        } catch (\Exception $exception) {
            $this->task->update([
                'status' => Task::STATUS_ERROR,
                'error_message' => $exception->getMessage()
            ]);
        }
    }
}
