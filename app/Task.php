<?php

namespace App;

use App\Jobs\DownloadResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Task extends Model
{
    /**
     * Pending status.
     * @var string
     */
    public const STATUS_PENDING = 'pending';

    /**
     * Downloading status.
     * @var string
     */
    public const STATUS_DOWNLOADING = 'downloading';

    /**
     * Complete status.
     * @var string
     */
    public const STATUS_COMPLETE = 'complete';

    /**
     * Error status.
     * @var string
     */
    public const STATUS_ERROR = 'error';

    /**
     * @var array
     */
    protected $fillable = ['url', 'resource', 'status', 'error_message'];

    /**
     * Get a download url for the downloaded resource.
     *
     * @return string
     */
    public function getDownloadUrlAttribute(): string
    {
        return Storage::url($this->resource);
    }

    /**
     * Generate a filename for the resource.
     *
     * @return string
     */
    public function generateResourceName(): string
    {
        $extension = pathinfo($this->url, PATHINFO_EXTENSION);
        if (empty($extension)) {
            $extension = 'html';
        }

        return sha1(time() . $this->url . rand(1, 10)) .'.'. $extension;
    }

    /**
     * Is task pending
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->status == self::STATUS_PENDING;
    }

    /**
     * Is task complete
     *
     * @return bool
     */
    public function isComplete(): bool
    {
        return $this->status == self::STATUS_COMPLETE;
    }

    /**
     * Is task in an error state
     *
     * @return bool
     */
    public function isError(): bool
    {
        return $this->status == self::STATUS_ERROR;
    }

    /**
     * Is task downloading
     *
     * @return bool
     */
    public function isDownloading(): bool
    {
        return $this->status == self::STATUS_DOWNLOADING;
    }

    /**
     * Store a new task in the database.
     *
     * @param Task $task
     * @param string $url
     * @return Task
     */
    public static function store(Task $task, string $url): Task
    {
        $task->url = $url;
        $task->status = Task::STATUS_PENDING;
        $task->save();

        dispatch(new DownloadResource($task));

        return $task;
    }
}
