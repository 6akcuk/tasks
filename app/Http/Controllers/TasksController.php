<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Repositories\TaskRepository;
use App\Task;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TasksController extends Controller
{
    /**
     * Show all tasks.
     *
     * @return View
     */
    public function index(): View
    {
        $tasks = Task::latest()->get();

        return view('tasks.index', compact('tasks'));
    }

    /**
     * Create a new task.
     *
     * @param Task $task
     * @param TaskRequest $request
     * @return RedirectResponse
     */
    public function store(Task $task, TaskRequest $request): RedirectResponse
    {
        Task::store($task, $request->url);

        session()->flash('message', 'Task has been created');
        return back();
    }

    /**
     * Delete a task.
     *
     * @param Task $task
     * @return RedirectResponse
     */
    public function destroy(Task $task)
    {
        $task->delete();

        session()->flash('message', 'Task has been removed.');

        return back();
    }
}
