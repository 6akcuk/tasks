<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Task;
use Illuminate\Database\Eloquent\Collection;

class TasksController extends Controller
{
    /**
     * List all tasks.
     *
     * @return Collection
     */
    public function index(): Collection
    {
        return Task::latest()->get();
    }

    /**
     * Store a new task in the database.
     *
     * @param Task $task
     * @param TaskRequest $request
     * @return Task
     */
    public function store(Task $task, TaskRequest $request): Task
    {
        return Task::store($task, $request->url);
    }

    /**
     * Delete a task.
     *
     * @param Task $task
     * @return bool|null
     */
    public function destroy(Task $task): string
    {
        $task->delete();

        return 'ok';
    }
}