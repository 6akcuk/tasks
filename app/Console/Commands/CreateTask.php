<?php

namespace App\Console\Commands;

use App\Task;
use Illuminate\Console\Command;

class CreateTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:create {url : Resource url to download}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new download task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $task = Task::store(new Task, $this->argument('url'));

        $this->info('Task has been created #'. $task->id);
    }
}
