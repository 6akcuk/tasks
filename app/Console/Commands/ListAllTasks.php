<?php

namespace App\Console\Commands;

use App\Task;
use Illuminate\Console\Command;

class ListAllTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::latest()->get()->map(function ($task) {
            return collect($task)->only(['id', 'url', 'status', 'error_message']);
        });

        $headers = ['ID', 'Resource Url', 'Status', 'Message'];
        $this->table($headers, $tasks);
    }
}
