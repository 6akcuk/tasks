<?php

namespace App\Console\Commands;

use App\Task;
use Illuminate\Console\Command;

class GetTaskResourceUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:download {id : Task id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get download url for the task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(Task::findOrFail($this->argument('id'))->download_url);
    }
}
